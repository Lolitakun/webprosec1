<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function storeLong(Request $request){
        $books = new Books();
        $books->setAttribute("name",$request->name);
        $books->setAttribute("category",$request->category);
        $books->setAttribute("description",$request->description);
    }
    public function storeShort(Request $request){
        $books = new Books();
        if(Books::Create($request->all())){
            return true;
        }
    }
    public function update(Request $request,Books $books){
        if($books->fill($request->all())->save()){
            return true;
        }
    }
    public function index(){
        $books = Books::all;
        return $books;
    }
    public function show(Books $books){
        return $books;
    }
    public function destroy(Books $books){
        if($books->delete()){
            return true;
        }
    }
}
