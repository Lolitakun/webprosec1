<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = 'Books'; // <- ORM
    protected $primaryKey = "isbn_no";
    protected $fillable = ['name','catagory','description'];
    //
}
