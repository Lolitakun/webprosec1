<?php
use illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        \Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'phatthanaphong',
            'username' => 'joe',
            'email'    => 'joe@smsu.ac.th',
            'password' => Hash::make('awesome'),
        ));
    }

}
