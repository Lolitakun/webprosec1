<?php
	// $comment = "<h1>Hey there! How are you doing today?</h1>";
	// // Delete All Tag HTML
	// $sanitizedComment = filter_var($comment, FILTER_SANITIZE_STRING);
	// echo $sanitizedComment;
	// echo $comment;

	// $int = 20.1;
	// if(filter_var($int, FILTER_VALIDATE_INT)){
	// 	echo "The <b>$int</b> is a valid integer";
	// }
	// else{
	// 	echo "The <b>$int</b> is not a valid integer";
	// }

	// $email = "asaneesar@msu.ac.th";

	// $email = filter_var($email, FILTER_SANITIZE_EMAIL);

	// if (filter_var($email, FILTER_VALIDATE_EMAIL)){
	// 	echo "The <b>$email<?b> is a valid email address";
	// }
	// else{
	// 	echo "The <b>$email<?b> is not a valid email address";
	// }

	$url = "https://www.example.com";

	$url = filter_var($url, FILTER_SANITIZE_URL);

	if(filter_var($url, FILTER_VALIDATE_URL)){
		echo "The <b>$url</b> is a valid website url";
	}
	else{
		echo "The <b>$url</b> is not a valid website url";
	}
?>