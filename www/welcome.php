<?php
	session_start();
	if(!isset($_SESSION['student_id'])){
		header( "location: login.php" );
	}
	require_once("connect.php");
?>
<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Welcome</title>
</head>
<body>
	<body>
		<h1>Welcome</h1>
		<table border="1" style="width:300px;">
			<tbody>
				<tr>
					<td>ID</td>
					<td><?php echo $_SESSION['student_id']?></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td><?php echo $_SESSION['first_name']?></td>
				</tr>
				<tr>
					<td>Last name</td>
					<td><?php echo $_SESSION['last_name']?></td>
				</tr>
			</tbody>			
		</table>
		<h1>History Registry</h1>
		<a href="register.php" title=""><h3>Add Subject</h3></a>
		<table border="1" style="width:300px;">
			<thead>
				<tr>
					<th>#</th>
					<th>Subject</th>
					<th>Credit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$studentid = $_SESSION['student_id'];
					$strSql = "SELECT cr_regist.r_id , cr_subject.sj_name , cr_subject.sj_credit 
							   FROM cr_regist,cr_subject 
							   WHERE cr_regist.r_s_id = '".$studentid."' and cr_regist.r_sj_id = cr_subject.sj_id ";

					$objQuery = mysqli_query($con,$strSql);	
				
					if(mysqli_num_rows($objQuery)>0){

						while ($x = mysqli_fetch_array($objQuery)) {
				?>			
							<tr>
								<td>#</td>
								<td><?php echo $x['sj_name']; ?></td>
								<td><?php echo $x['sj_credit']; ?></td>
								<td><a href="deletesubject.php?id=<?php echo $x['r_id']; ?>" title="">delete</a></td>
							</tr>
				<?php
						}
					}
					else{
				?>
						<tr>
							<td colspan="4"><center>Data Not Found</center></td>
						</tr>
				<?php
					}
				?>
			</tbody>
		</table>
		<br>
		<a href="logout.php" title="">Logout</a>
	</body>
</body>
</html>
<?php
	mysqli_close($con);
?>