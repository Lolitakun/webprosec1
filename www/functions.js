function makeUser(identity,nameValue,ageValue,SexValue,AddressValue,TelValue,EMailValue){
	return{
		id: identity,
		name: nameValue,
		age: ageValue,
		Sex: SexValue,
		Address: AddressValue,
		Tel: TelValue,
		EMail: EMailValue
	}
}

function getUser(User){
	let str="";
	for(let key in User){
		str = str + User[key]+"\n";
	}
	return str;
}

function cloneUser(User){
	let tmp_user = {};
	for(let key in User){		
		tmp_user[key] = User[key];
	}
	return tmp_user;
}